using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ByodLauncher.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SignInResult = ByodLauncher.Models.SignInResult;

namespace ByodLauncher.Services
{
    public class SignInManager
    {
        private readonly ILogger<SignInManager> _logger;
        private readonly ByodLauncherContext _context;
        private readonly JwtAuthService _jwtAuthService;
        private readonly JwtTokenConfiguration _jwtTokenConfiguration;

        public SignInManager(
            ILogger<SignInManager> logger,
            ByodLauncherContext context,
            JwtAuthService jwtAuthService,
            JwtTokenConfiguration jwtTokenConfiguration
        )
        {
            _logger = logger;
            _context = context;
            _jwtAuthService = jwtAuthService;
            _jwtTokenConfiguration = jwtTokenConfiguration;
        }

        public async Task<SignInResult> SignIn(string username)
        {
            SignInResult result = new SignInResult();

            if (string.IsNullOrWhiteSpace(username)) return result;

            var user = await _context.Users.Where(u => u.UserName == username).FirstOrDefaultAsync();
            if (user != null)
            {
                var claims = BuildClaims(user);
                result = result with
                {
                    User = user,
                    AccessToken = _jwtAuthService.BuildToken(claims),
                    RefreshToken = _jwtAuthService.BuildRefreshToken()
                };

                var refreshToken = new RefreshToken
                {
                    User = user,
                    Token = result.RefreshToken,
                    IssuedAt = DateTime.Now,
                    ExpiresAt = DateTime.Now.AddMinutes(_jwtTokenConfiguration.RefreshTokenExpiration)
                };
                _context.RefreshTokens.Add(refreshToken);
                await _context.SaveChangesAsync();

                result = result with {Success = true};
            }

            return result;
        }

        public async Task<SignInResult> RefreshToken(string accessToken, string refreshToken)
        {
            ClaimsPrincipal principal = _jwtAuthService.GetPrincipalFromToken(accessToken);
            SignInResult result = new SignInResult();

            if (principal == null) return result;

            string userId = principal.Claims.First(claim => claim.Type == "id").Value;
            var user = await _context.Users.FindAsync(Guid.Parse(userId));

            if (user == null) return result;

            var token = await _context.RefreshTokens.Where(
                                          t => t.UserId == user.Id
                                               && t.Token == refreshToken
                                               && t.ExpiresAt >= DateTime.Now)
                                      .FirstOrDefaultAsync();

            if (token == null) return result;

            var claims = BuildClaims(user);

            result = result with
            {
                User = user,
                AccessToken = _jwtAuthService.BuildToken(claims),
                RefreshToken = _jwtAuthService.BuildRefreshToken()
            };

            _context.RefreshTokens.Remove(token);
            var newRefreshToken = new RefreshToken
            {
                User = user,
                Token = result.RefreshToken,
                IssuedAt = DateTime.Now,
                ExpiresAt = DateTime.Now.AddMinutes(_jwtTokenConfiguration.RefreshTokenExpiration)
            };
            _context.RefreshTokens.Add(newRefreshToken);
            await _context.SaveChangesAsync();

            result = result with {Success = true};
            return result;
        }

        private IEnumerable<Claim> BuildClaims(ByodUser user)
        {
            var claims = new[]
            {
                new Claim("id", user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
            };

            return claims;
        }
    }
}