using System;

namespace ByodLauncher.Models
{
    public class DirectorSession
    {
        public Guid Id { get; set; }
        public Guid DirectorId { get; set; }
        public Director Director { get; set; }
        public DateTime CreationDate { get; set; }
    }
}