﻿using System;
using System.Text.Json.Serialization;
using ByodLauncher.Utilities;

namespace ByodLauncher.Models.Dto
{
    public class SessionSummaryDto
    {
        [JsonConverter(typeof(GuidConverter))] public Guid Id { get; set; }
        public string Title { get; set; }
        public DirectorDto Director { get; set; }
        // public DateTime CreationDate { get; set; }
    }
}
