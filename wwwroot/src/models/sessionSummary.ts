import {Id} from "@/models/idType";
import { Director } from "./director";

export class SessionSummary {
    id: Id = null;
    title = '';
    director: Director = new Director();
    creationDate: Date = new Date();

    public constructor(init?: Partial<SessionSummary>) {
        Object.assign(this, init);
    }

    public update(updateData: Partial<SessionSummary>) {
        Object.assign(this, updateData);
    }
}