import { AxiosError, AxiosResponse } from "axios";
import Api from "@/api/api";
import { Session } from "@/models/session";
import { Id } from "@/models/idType";
import { SessionSummary } from "@/models/sessionSummary";

class SessionApi extends Api {
	public constructor() {
		super();

		this.createSession = this.createSession.bind(this);
		this.updateSession = this.updateSession.bind(this);
	}

	public createSession(session: Session): Promise<Session> {
		return this.post<Session>("session", JSON.stringify(session))
			.then((response: AxiosResponse<Session>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public updateSession(session: Session): Promise<Session> {
		return this.put<Session>(
			`session/${session.id}`,
			JSON.stringify(session)
		)
			.then((response: AxiosResponse<Session>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public getSession(sessionId: Id): Promise<Session> {
		return this.get<Session>(`session/${sessionId}`)
			.then((response: AxiosResponse<Session>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public getUserSessions(): Promise<Session[]> {
		return this.get<Session[]>(`session/userSession`)
			.then((response: AxiosResponse<Session[]>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public async getPublishedSessions(): Promise<SessionSummary[]> {
		return this.get<SessionSummary[]>(`session/published`)
			.then((response: AxiosResponse<SessionSummary[]>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public getSessionsByName(name: string): Promise<SessionSummary[]> {
		return this.get<SessionSummary[]>(`session/filter?name=${name}`)
			.then((response: AxiosResponse<SessionSummary[]>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public getByAccessCode(accessCode: string): Promise<Session[]> {
		return this.get<Session[]>(`session?accessCode=${accessCode}`)
			.then((response: AxiosResponse<Session[]>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public copySession(sessionId: Id): Promise<Session> {
		return this.get<Session>(`session/copy/${sessionId}`)
			.then((response: AxiosResponse<Session>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}

	public getByEditCode(editCode: string): Promise<Session[]> {
		return this.get<Session[]>(`session?editCode=${editCode}`)
			.then((response: AxiosResponse<Session[]>) => {
				return response.data;
			})
			.catch((error: AxiosError) => {
				throw error;
			});
	}
}

export const sessionApi = new SessionApi();
